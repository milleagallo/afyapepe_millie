@extends('layouts.doctor_layout')
@section('title', 'Patient History')
@section('content')


@section('styles')
<link rel="stylesheet" href="{{ asset('css/plugins/slick/slick.css') }}" />
<link rel="stylesheet" href="{{ asset('css/plugins/slick/slick-theme.css') }}" />

@endsection
<?php $app_id =$appoint->id;
      $afyauserId = $appoint->afya_user_id;
      $condition = $appoint->condition;
 ?>
@section('leftmenu')
@include('includes.doc_inc.leftmenu2')
@endsection

<div class="row wrapper white-bg page-heading">
              <div class="col-lg-8">
                  <h2>MEDICAL REPORT</h2>
              </div>
              <div class="col-lg-4">
                  <div class="title-action">

                    <a href="{{url('doctor.history',$app_id)}}"  class="btn btn-primary"><i class="fa fa-angle-double-left"></i> BACK </a>

                  </div>
              </div>
          </div>

          <div class="wrapper wrapper-content">

              <div class="row">
                  <div class="col-lg-10 col-lg-offset-1">
                      <div class="ibox">

                          <div class="slick_demo_1">




                            @foreach($appointments as $apps)
<?php
$ptdetails= DB::table('triage_details')->where('appointment_id',$apps->id)->first();

$user = DB::table('appointments')
->Join('afya_users', 'appointments.afya_user_id', '=', 'afya_users.id')
->leftJoin('dependant', 'appointments.persontreated', '=', 'dependant.id')
->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
->select('afya_users.firstname','afya_users.secondName','afya_users.dob','afya_users.gender','afya_users.occupation',
'afya_users.age','appointments.persontreated','appointments.id as appid','afya_users.id as afyaId','patient_admitted.condition')
->where('appointments.id', '=',$apps->id)->first();


$doct= DB::table('appointments')
->leftJoin('doctors', 'appointments.doc_id', '=', 'doctors.id')
->leftJoin('facilities', 'appointments.facility_id', '=', 'facilities.FacilityCode')
->select('doctors.name','facilities.FacilityName','appointments.created_at','appointments.updated_at')
->where('appointments.id', '=',$apps->id)
->first();
$timestamp = strtotime($apps->created_at);
$date1= date("jS", $timestamp);
$date2= date("D F y", $timestamp);

$timestampt1 = strtotime($apps->created_at);
$Time1= date("h:i:s A", $timestampt1);
$intervalT = date_diff(date_create($apps->updated_at), date_create($apps->created_at));
$Time2= $intervalT->format("%h Hour :%i mins");

$summary = DB::table('patient_summary')->select('notes')->where('appointment_id', '=',$apps->id)->get();
$cmeds= DB::table('current_medication')->select('drugs')->where('appointment_id', '=',$apps->id)->get();
$ge= DB::table('general_examination')->where('appointment_id', '=',$apps->id)->first();

$tsts = DB::table('appointments')
    ->leftJoin('patient_test', 'patient_test.appointment_id', '=', 'appointments.id')
    ->leftJoin('patient_test_details', 'patient_test.id', '=', 'patient_test_details.patient_test_id')
    ->leftJoin('icd10_option', 'patient_test_details.conditional_diag_id', '=', 'icd10_option.id')
    ->leftJoin('tests', 'patient_test_details.tests_reccommended', '=', 'tests.id')
    ->leftJoin('test_subcategories', 'tests.sub_categories_id', '=', 'test_subcategories.id')
    ->leftJoin('test_categories', 'test_subcategories.categories_id', '=', 'test_categories.id')
    ->leftJoin('test_results', 'patient_test_details.id', '=', 'test_results.id')
    ->select('tests.name as tname','test_results.value')
    ->where('appointments.id', '=',$apps->id)
    ->get();

    $rady = DB::table('patient_test')
       ->Join('radiology_test_details', 'patient_test.id', '=', 'radiology_test_details.patient_test_id')
       ->leftJoin('test_categories', 'radiology_test_details.test_cat_id', '=', 'test_categories.id')
       ->select('radiology_test_details.created_at as date','radiology_test_details.test',
       'radiology_test_details.clinicalinfo','radiology_test_details.test_cat_id','radiology_test_details.done',
       'radiology_test_details.id as patTdid','test_categories.name as tcname')
       ->where('patient_test.appointment_id', '=',$apps->id)
        ->get();

        $prescriptions =DB::table('prescriptions')
                     ->join('prescription_details','prescriptions.id','=','prescription_details.presc_id')
                    ->select('prescription_details.drug_id')
                    ->where('prescriptions.appointment_id',$apps->id)
                     ->get();

$diagnosis = DB::table('patient_diagnosis')
->select('disease_id')
->where('appointment_id', '=',$apps->id)
->orderBy('appointment_id', 'desc')
->get();

$procedures = DB::table('appointments')
->Join('patient_procedure', 'appointments.id', '=', 'patient_procedure.appointment_id')
->Join('procedures', 'patient_procedure.procedure_id', '=', 'procedures.id')
->Join('facilities', 'appointments.facility_id', '=', 'facilities.FacilityCode')
->select('procedures.description','patient_procedure.procedure_date','patient_procedure.notes','facilities.FacilityName')
->where('appointments.id', '=',$apps->id)
->orderBy('appointments.id', 'desc')
->get();
 ?>
                            <div>
                                <div class="ibox-content">
                                    <table class="table table-bordered">

                                      <tr> <td width="15%"><h3>{{$date1}} </h3>{{$date2}} </td>
                                        <td colspan="2" width="40%">{{$doct->name}}  <br> {{$doct->FacilityName}} <br>At {{$Time1}} For {{$Time2}} </td>
                                        <td  width="45%"> <p class="pull-right">{{$user->firstname}}  {{$user->secondName}}<br> {{$user->gender}} <br> {{$user->age}}</p></td>
                                      </tr>


                                  @if($ptdetails) <tr>
                                    <td colspan="2">TRIAGE</td><td colspan="2">
                                    <div class="col-xs-3">
                                    <small class="stats-label">Height </small>
                                    <strong> {{$ptdetails->current_height}}cm</strong>
                                    </div>
                                    <div class="col-xs-3">
                                    <small class="stats-label">Weight </small>
                                    <strong> {{$ptdetails->current_weight}}kg</strong>
                                    </div>
                                    <div class="col-xs-3">
                                    <small class="stats-label">Temperature </small>
                                    <strong> {{$ptdetails->temperature}}°C</strong>
                                    </div>
                                    <div class="col-xs-3">
                                    <small class="stats-label">HR </small>
                                    <strong> {{$ptdetails->hr}}b/min</strong>
                                    </div>

                                    <div class="col-xs-3">
                                    <small class="stats-label">BP</small>
                                    <strong> @if($ptdetails->systolic_bp){{$ptdetails->systolic_bp}} / {{$ptdetails->diastolic_bp}}mmHg @endif</strong>
                                    </div>

                                    <div class="col-xs-3">
                                    <small class="stats-label">SpO2</small>
                                    <strong> {{$ptdetails->rr}}breaths/min</strong>
                                    </div>

                                    <div class="col-xs-3">
                                    <small class="stats-label">RBS </small>
                                    <strong> {{$ptdetails->rbs}}mmol/l </strong>
                                    </div>
                                        </td>
                                      </tr>
                                      @endif
                                      @if($summary)
                                      <tr>
                                       <td colspan="2">PATIENT SUMMARY</td><td colspan="2">
                                       @foreach($summary as $summ)
                                           {{$summ->notes}} <br>
                                        @endforeach</td>
                                      </tr>
                                      @endif
                                      @if($cmeds)
                                      <tr>
                                      <td colspan="2">CURRENT MEDICATION</td><td colspan="2">
                                        @foreach($cmeds as $cmed)
                                            {{$cmed->drugs}} <br>
                                         @endforeach</td>
                                      </tr>
                                      @endif
                                      @if($ge)
                                      <tr>
                                      <td colspan="2">EXAMINATION FINDINGS</td>
                                      <td colspan="2">
                                        <div class="col-xs-3">
                                        <small class="stats-label">GENERAL EXAMINATION</small>
                                        <strong>{{$ge->g_examination}}</strong>
                                        </div>
                                        <div class="col-xs-3">
                                        <small class="stats-label">CVS</small><br>
                                        <strong>{{$ge->cvs}}</strong>
                                        </div>
                                        <div class="col-xs-3">
                                        <small class="stats-label">RS</small><br>
                                        <strong>{{$ge->rs}}</strong>
                                        </div>
                                        <div class="col-xs-3">
                                        <small class="stats-label">PA</small><br>
                                        <strong>{{$ge->pa}}</strong>
                                        </div>
                                        <div class="col-xs-3">
                                        <small class="stats-label">CNS</small><br>
                                        <strong>{{$ge->cns}}</strong>
                                        </div>
                                        <div class="col-xs-3">
                                        <small class="stats-label">MSS</small><br>
                                        <strong>{{$ge->mss}}</strong>
                                        </div>
                                        <div class="col-xs-3">
                                        <small class="stats-label">PERIPHERIES</small><br>
                                        <strong>{{$ge->peripheries}}</strong>
                                        </div>
                                      </td>
                                      </tr>
                                      @endif
                                      @if($diagnosis)
                                      <tr>
                                      <td colspan="2">DIAGNOSIS</td><td colspan="2">
                                         @foreach($diagnosis as $tst1)
                                        {{$tst1->disease_id}} <br>
                                        @endforeach</td>
                                      </tr> @endif

                                      
                                      @if($tsts)
                                      <tr>
                                      <td colspan="2">TESTS</td><td colspan="2">
                                      @foreach($tsts as $tst)
                                      {{$tst->tname}}  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  {{$tst->value}}</br>
                                      @endforeach
                                      </td>
                                      <tr>
                                      @endif
                                        @if($rady)
                                      <td colspan="2">TESTS</td><td colspan="2">

                                                                    @foreach($rady as $radst)
                                                                    <?php
                                                                    $test = '';
                                                                    $result= '';
                                                                    if ($radst->test_cat_id== '9') {
                                                                      $ct=DB::table('ct_scan')->where('id', '=',$radst->test) ->first();
                                                                      $ctresult=DB::table('radiology_test_result')->where('radiology_td_id', '=',$radst->patTdid) ->first();
                                                                      $test = $ct->name;
                                                                    if($ctresult){  $result = $ctresult->results; }
                                                                    } elseif ($radst->test_cat_id== 10) {
                                                                      $xray=DB::table('xray')->where('id', '=',$radst->test) ->first();
                                                                      $xrayresult=DB::table('radiology_test_result')->where('radiology_td_id', '=',$radst->patTdid) ->first();
                                                                      $test = $xray->name;
                                                                    if($xrayresult){  $result = $xrayresult->results; }
                                                                    } elseif ($radst->test_cat_id== 11) {
                                                                      $mri=DB::table('mri_tests')->where('id', '=',$radst->test)->first();
                                                                      $mriresult=DB::table('radiology_test_result')->where('radiology_td_id', '=',$radst->patTdid) ->first();
                                                                      $test = $mri->name;
                                                                if($mriresult)  { $result = $mriresult->results; }
                                                                    }elseif ($radst->test_cat_id== 12) {
                                                                      $ultra=DB::table('ultrasound')->where('id', '=',$radst->test) ->first();
                                                                      $ultraresult=DB::table('radiology_test_result')->where('radiology_td_id', '=',$radst->patTdid) ->first();
                                                                      $test = $ultra->name;
                                                                    if($ultraresult) { $result = $ultraresult->results;  }
                                                                    }elseif ($radst->test_cat_id== 13) {
                                                                      $other=DB::table('other_tests')->where('id', '=',$radst->test) ->first();
                                                                      $otherresult=DB::table('radiology_test_result')->where('radiology_td_id', '=',$radst->patTdid) ->first();
                                                                      $test = $other->name;
                                                                    if($otherresult) { $result = $otherresult->results;}
                                                                    }


                                                                    ?>
                                      {{$test}}   @if($result) <strong>&nbsp;&nbsp;&nbsp;&nbsp; RESULTS &nbsp;&nbsp;&nbsp;&nbsp;</strong>  {{$result}} @endif</br>
                                      @endforeach
                                      </td>
                                      </tr>
                                      @endif
                                      @if($procedures)
                                      <tr>
                                      <td colspan="2">Procedures :</td><td colspan="2">
                                      @foreach($procedures as $prc)
                                      {{$prc->description}} <br>
                                      @endforeach</td>
                                      </tr>
                                      @endif
                                      @if($prescriptions)
                                      <tr>
                                      <td colspan="2">Prescriptions :</td><td colspan="2">
                                      @foreach($prescriptions as $prsc)
                                      {{$prsc->drug_id}}<br>
                                      @endforeach</td>
                                      </tr>
                                      @endif

                                    </table>
                                    </div>
                                  </div>




                       @endforeach


                      </div>
                  </div>
              </div>
</div>

























    @endsection
    @section('script-test')
    <!-- slick carousel-->
        <script src="{{ asset('js/plugins/slick/slick.min.js') }}" type="text/javascript"></script>

        <!-- Additional style only for demo purpose -->
        <script>
            $(document).ready(function(){
                $('.slick_demo_1').slick({
                    dots: true
                });
            });
      </script>


    @endsection
