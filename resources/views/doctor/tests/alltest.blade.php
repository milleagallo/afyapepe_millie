@extends('layouts.doctor_layout')
@section('title', 'Test')
@section('styles')
<link rel="stylesheet" href="{{asset('css/custombuttons.css') }}" />
@endsection
@section('content')
<?php
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){


$Did = $Docdata->id;
$Name = $Docdata->name;
$Address = $Docdata->address;
$RegNo = $Docdata->regno;
$RegDate = $Docdata->regdate;
$Speciality = $Docdata->speciality;
$Sub_Speciality = $Docdata->subspeciality;


}
      $stat= $pdetails->status;
         $afyauserId= $pdetails->afya_user_id;
          $dependantId= $pdetails->persontreated;
          $app_id_prev= $pdetails->last_app_id;
          $app_id =  $pdetails->id;
          $doc_id= $pdetails->doc_id;
          $fac_id= $pdetails->facility_id;
          $fac_setup= $pdetails->set_up;
          $dependantAge = $pdetails->depdob;
          $AfyaUserAge = $pdetails->dob;
          $condition = $pdetails->condition;


?>
@section('leftmenu')
@include('includes.doc_inc.leftmenu2')
@endsection
@include('includes.doc_inc.topnavbar_v2')



        <!--tabs Menus-->
<!-- <div class="row wrapper border-bottom">
    <div class="float-e-margins">
          <div class="col-md-10">


<div class="well text-center col-md-12">
  <p class="text-left"> All Tests results </p>
  <a href="{{url('test-all',$app_id)}}" class="btn btn-w-m btn-primary">TEST RESULTS</a>

</div>
<p class="text-center">
  Select tests to recommend
</p>
      <div class="well text-center col-md-6">
        <a href="{{route('testes',$app_id)}}" class="btn btn-w-m btn-primary">LAB TESTS</a>
      </div>

      <div class="well text-center col-md-6">
        <a href="{{route('testesmri',$app_id)}}" class="btn btn-w-m btn-primary">MRI TESTS</a>
          <a href="{{route('otherimaging',$app_id)}}" class="btn btn-w-m btn-primary">OTHER IMAGING TESTS</a>
      </div>

      <div class="well text-center col-md-6">
        <a href="{{route('testesImage',$app_id)}}" class="btn btn-w-m btn-primary">CT-SCAN TESTS</a>
      </div>


  <div class="well text-center col-md-6">
        <a href="{{route('testesultra',$app_id)}}" class="btn btn-w-m btn-primary">ULTRASOUND TESTS</a>
      </div>

      <div class="well text-center col-md-6">
        <a href="{{route('testesxray',$app_id)}}" class="btn btn-w-m btn-primary">XRAY TESTS</a>
      </div>

      <div class="well text-center col-md-6">
        <a href="#" class="btn btn-w-m btn-primary">Neurology </a>
        <a href="#" class="btn btn-w-m btn-primary">Gastrointestinal</a>
      </div>



      </div>
   </div>
   </div> -->

@include('doctor.tests.testmodals')

@endsection

@section('script-test')
  <!-- Page-Level Scripts -->
  <script src="{{ asset('js/reg_test.js') }}"></script>
  @endsection
