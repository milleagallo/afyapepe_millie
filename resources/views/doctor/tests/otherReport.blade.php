@extends('layouts.doctor_layout')
@section('title', 'Test')
@section('content')
<?php
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){


$Did = $Docdata->id;
$Name = $Docdata->name;
$Address = $Docdata->address;
$RegNo = $Docdata->regno;
$RegDate = $Docdata->regdate;
$Speciality = $Docdata->speciality;
$Sub_Speciality = $Docdata->subspeciality;


}

   $stat= $patientD->status;
         $afyauserId= $patientD->afya_user_id;
          $dependantId= $patientD->persontreated;
          $app_id =  $patientD->id;
          $doc_id= $patientD->doc_id;
          $fac_id= $patientD->facility_id;
          $fac_setup= $patientD->set_up;
          $condition  = $patientD->condition;


 if ($dependantId =='Self') {
          $dob=$patientD->dob;
          $gender=$patientD->gender;
          $firstName = $patientD->firstname;
          $secondName = $patientD->secondName;
          $name =$firstName." ".$secondName;

   }else {
           $dependantId=$patientD->persontreated;
//Dependant data to be here

      }
  $interval = date_diff(date_create(), date_create($dob));
  $age= $interval->format(" %Y Year, %M Months, %d Days Old");
?>
        <!--tabs Menus-->
        @section('leftmenu')
        @include('includes.doc_inc.leftmenu2')
        @endsection
        @include('includes.doc_inc.topnavbar_v2')

     <div class="row wrapper border-bottom white-bg page-heading">

     	<div class="row">
     			<div class="col-md-12">

     			<div class="col-md-6">
     				<address>
              <br />
     				<strong>Patient:</strong><br>
     				Name: {{$name}}<br>
     				Gender: {{$gender}}<br>
     				Age: {{$age}}
           </address>

     			</div>
     			<div class="col-md-6 text-right">
            <?php
            $datetime = explode(" ",$tsts1->created_at);
            $date1 = $datetime[0];
            ?>
     				<address>
              <br />
     					<strong>Requested By:</strong><br>
     					Doctor :{{$tsts1->docname}} <br>
     					LAB :  {{$tsts1->FacilityName}} <br>
              Date : {{$date1}} <br>


     				</address>
     			</div>
     		</div>
     </div>
<?php
 // $images=DB::table('radiology_images')->where('radiology_td_id',$tsts1->id)->get();
 ?>
{{--
     <form role="form" class="form-inline">
 @foreach($images as $image)
    <div class="form-group">
   <a href="{{asset("images/$image->image") }} "target="_blank">View Image</a>
 @endforeach
      </form>
      --}}
<?php
$tst21 = DB::table('radiology_test_result')
->select('results')
->where('radiology_td_id', '=',$tsts1->id)
->first();
 ?>
    <div class="col-md-12">
              <div class="ibox float-e-margins">

                  <div class="ibox-title">
                    <h5>TEST RESULTS</h5>
                    <div class="ibox-tools">
                      <a class="btn btn-primary"  href="{{url('test-all',$app_id)}}"><i class="fa fa-angle-double-left"></i>&nbsp;BACK</a>
                    </div>
                  </div>
                  <div class="ibox-content">
                    <h3 class="text-center"></h3>
                      <div class="list-group">
                        {{ Form::open(array('route' => array('doc.otherResult'),'method'=>'POST')) }}
                              <div class="col-md-10">
                              <div class="form-group">
                                <label>TEST:</label>
                                <input type="text" name="radiology" value="{{$tsts1->name}}" class="form-control" readonly/>
                              </div>

                              @if($tsts1->clinicalinfo) <div class="form-group">
                                    <label>CLINICAL INFORMATION:</label>
                                    <input type="text"  value="{{$tsts1->clinicalinfo}}" class="form-control" readonly/>
                                  </div>
                              @endif
              @if($tsts1->technique)
                                  <div class="form-group">
                                    <label>TECNIQUE:</label>
                                    <input type="text" name="technique" value="{{$tsts1->technique}}" class="form-control" readonly/>
                                  </div>
                                  @else
                                  <div class="form-group">
                                    <label>TECHNIQUE:</label>
                                    <input type="text" name="technique" value="{{$tsts1->technique}}" class="form-control"/>
                                  </div>
                                  @endif

                                 <div class="form-group">
                                   <label>Results :</label></br>
                                   <textarea rows="6" name="note" cols="100">@if($tst21){{$tst21->results}} @endif</textarea>
                                 </div>
                               {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
                               {{ Form::hidden('rtdid',$tsts1->id, array('class' => 'form-control')) }}
                               </div>
                               @if($tst21)

                                @else

                               <div class="col-md-12">
                               <button class=" mtop btn btn-sm btn-primary  m-t-n-xs" type="submit"><strong>Submit</strong></button>
                               </div>
                                 @endif
                               {{ Form::close() }}
                  </div>
              </div>
          </div>
        </div>

</div>





@endsection
