<?php

$triage = DB::table('triage_details')->where('appointment_id',$app_id)
->first();
  ?>
<div class="wrapper wrapper-content">

   <div class="col-lg-12">
                       <div class="ibox float-e-margins">
                           <div class="ibox-title">
                               <h5>Today's Vitals </h5>
                               <div class="ibox-tools">
                               </div>
                           </div>
                           <div class="ibox-content">
                   <div class="row">
                   {!! Form::open(array('route' => 'trgpost','method'=>'POST')) !!}
                     <div class="col-lg-3 b-r">
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">

                         <input type="hidden" class="form-control"  value="{{$afyauserId}}" name="id">
                         <input type="hidden" class="form-control" value="{{$app_id}}" name="appointment_id"  >

                       <div class="form-group">
                       <label for="exampleInputEmail1">Weight (kg)</label>
                       <input type="text" class="form-control"  value="@if($triage){{$triage->current_weight}} @endif" name="weight"  >
                       </div>

                       <div class="form-group">
                       <label for="exampleInputEmail1">Height (cm)</label>
                       <input type="text" class="form-control" value="@if($triage){{$triage->current_height}} @endif" name="current_height">
                       </div>
       </div>
        <div class="col-lg-3 br">
                      <div class="form-group">
                       <label for="exampleInputPassword1">Temperature °C</label>
                       <input type="text" class="form-control" value="@if($triage){{$triage->temperature}} @endif" name="temperature"  >
                      </div>

                       <div class="form-group">
                       <label for="exampleInputPassword1">Systolic BP</label>
                       <input type="text" class="form-control"  value="@if($triage){{$triage->systolic_bp}} @endif" name="systolic" >
                   </div>
</div>
 <div class="col-lg-3 br">
                       <div class="form-group">
                       <label for="exampleInputEmail1">Diastolic BP</label>
                       <input type="text" class="form-control"   value="@if($triage){{$triage->diastolic_bp}} @endif" name="diastolic">
                       </div>

                       <div class="form-group">
                       <label for="exampleInputEmail1">RBS mmol/l</label>
                       <input type="name" class="form-control"   value="@if($triage){{$triage->rbs}} @endif" name="rbs">
                       </div>




                     </div>

                     <div class="col-lg-3">

                       <div class="form-group">
                       <label for="exampleInputEmail1">HR b/min</label>
                       <input type="name" class="form-control"  value="@if($triage){{$triage->hr}} @endif" name="hr">
                       </div>

                       <div class="form-group">
                       <label for="exampleInputEmail1">SpO<small>2</small></label>
                       <input type="name" class="form-control"   value="@if($triage){{$triage->rr}} @endif" name="rr">
                       </div>
            <?php
             // if($triage){$comp = $triage->chief_compliant;}else{$comp = '';}
             //     if($triage){$sympt = $triage->symptoms;}else{$sympt = '';}
             //     if($triage){$observ = $triage->observation;}else{$observ = '';}
                         ?>

                         <!-- <div class="form-group">
                         <label >Chief Complaint:</label><br />
                         <select multiple="multiple" id="chief" name="chiefcomplaint[]" class="form-control chief" style="width:80%">
                           <option selected="selected"></option>
                         </select>
                         </div>
                         <div class="form-group">
                         <label >Observation:</label><br />
                         <select multiple="multiple" id="observation" name="observation[]" class="form-control chief" style="width:80%">
                           <option selected="selected"></option>
                         </select>
                         </div>
                         <div class="form-group">
                         <label >Symptom:</label><br />
                         <select multiple="multiple" id="symptom" name="symptoms[]" class="form-control chief" style="width:80%">
                           <option selected="selected"></option>
                         </select>
                         </div> -->
 </div>
  <!-- <div class="col-md-7"> -->
                         <!-- <div class="form-group">
                         <label for="exampleInputPassword1">Nurse Notes</label><br />
                         <textarea class="form-control"  name="nurse" >@if($triage){{$triage->Doctor_note}} @endif</textarea>
                         </div> -->
 <!-- </div> -->
  <div class="col-md-6">
                          <?php
                          $db = DB::table('afya_users')
                              ->where('id',$afyauserId)
                              ->first();

                          $gender = $db->gender;
                           ?>
                          @if($gender == 'Female')
                         <div class="form-group">
                        <label for="exampleInputPassword1">Pregnant?</label><br />
                        <input type="radio" value="No"  name="pregnant"  @if($triage) <?php  echo ($triage->pregnant=='No')?'checked':'' ?> @endif > No
                        <input type="radio" value="Yes"  name="pregnant" @if($triage) <?php echo ($triage->pregnant=='Yes')?'checked':'' ?> @endif > Yes

                        <!-- <input type="text" class="form-control"   value="@if($triage){{$triage->pregnant}} @endif" name="pregnant"> -->
                          </div>
    </div>
     <div class="col-md-6">
                          <div class="form-group">
                          <label for="exampleInputEmail1">LMP </label>
                          <input type="text" class="form-control"   value="@if($triage){{$triage->lmp}} @endif" name="lmp">
                          </div>

                          @endif

                   <button type="submit" class="btn btn-primary">@if($triage)UPDATE @else SUBMIT @endif</button>
                     </div>
                     {!! Form::close() !!}
                   </div>
 </div>
</div>
</div>



</div>
